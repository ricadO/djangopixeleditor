# Pixel-django

Editor de pixels com Django, não sei o que eu estou fazendo, pra ser bem sincero. Eu ví uma vaga que pedia conhecimento em Django e fui procurar o que era, e aqui estou.

Criado usando o [tutorial](https://docs.djangoproject.com/en/3.2/intro/tutorial01) do próprio site do Django.

### Comandos

Rodar o servidor na [porta :8000](http://127.0.0.1:8000):

```shell
python manage.py runserver
```

~Compilar~, digo ~buildar~, digo migrar tudo:

```shell
python manage.py makemigrations
```

```shell
python manage.py migrate
```

explorar o app com python

```
python manage.py shell
```

criar um admin:

```shell
python manage.py createsuperuser
```

### Fazer

- botão pra adicionar pixels
- selecionar vários pixels ao mesmo tempo
- campo de texto pra hex-color
- zoom
- deployar o app no heroku
