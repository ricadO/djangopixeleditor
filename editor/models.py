import datetime

from django.contrib import admin
from django.db import models
from django.utils import timezone

# Create your models here.
class Pixel(models.Model):
  posicao = models.IntegerField(default=0)
  cor = models.CharField(max_length=7)

  def __str__(self):
    return str(self.posicao)
