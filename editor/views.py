from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
from django.utils import timezone
from django.views import generic

from .models import Pixel

# Create your views here.
class IndexView(generic.ListView):
    template_name = 'editor/index.html'
    context_object_name = 'pixels_list'

    def get_queryset(self):
        """
        Retorna os pixels criados
        """
        return Pixel.objects.all()

class DetailView(generic.DetailView):
    model = Pixel
    template_name = 'editor/detail.html'

class ResultsView(generic.DetailView):
    model = Pixel
    template_name = 'editor/results.html'

def colorir(request, pixel_posicao):
    pixel = get_object_or_404(Pixel, pk=pixel_posicao)
    opcao = request.POST['cor']
        
    pixel.cor = opcao
    pixel.save()

    # Always return an HttpResponseRedirect after successfully dealing
    # with POST data. This prevents data from being posted twice if a
    # user hits the Back button.
    return HttpResponseRedirect(reverse('editor:index'))