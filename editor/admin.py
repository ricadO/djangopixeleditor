from django.contrib import admin

from .models import Pixel

# Register your models here.

class PixelAdmin(admin.ModelAdmin):
  model = Pixel
  extra = 3

admin.site.register(Pixel)
